package com.statefarm.autos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AutosRepository extends JpaRepository<Automobile, Long> {
    List<Automobile> findByColorContainsAndMakeContainsIgnoreCase(@Param("color") String color, @Param("make") String make);
    List<Automobile> findByColorContainsIgnoreCase(@Param("color") String color);
    List<Automobile> findByMakeContainsIgnoreCase(@Param("make") String make);
    Optional<Automobile> findByVin(String vin);
}
