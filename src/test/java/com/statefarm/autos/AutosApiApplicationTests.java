package com.statefarm.autos;

import org.apache.coyote.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AutosApiApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;
    RestTemplate patchRestTemplate;

    @Autowired
    AutosRepository autosRepository;

    Random r = new Random();
    List<Automobile> testAutos;
    @BeforeEach
    void setup() {
        this.testAutos = new ArrayList<>();
        String[] colors = {"RED", "BLUE", "GREEN", "PURPLE", "YELLOW", "BLACK", "CUB BLUE", "FOREST GREEN"};
        String[] makes = {"Honda", "Toyota", "Ford", "Chevy", "Dodge" };
        String[] owners = {"Alan", "Alesha", "Brandon", "Brendan", "Chris K", "Chris W", "David", "Jasmine", "Jessi", "Josh", "Matt", "Scott", "Yogurt"};
        HashMap<String, String[]> models = new HashMap<>();
        models.put(makes[0], new String[] {"Civic", "Accord", "NSX", "CR-V", "Odyssey"});
        models.put(makes[1], new String[] {"Camry", "Corolla", "Sienna", "Supra", "Tacoma"});
        models.put(makes[2], new String[] {"Mustang", "F-150", "Bronco", "GT", "Taurus"});
        models.put(makes[3], new String[] {"Suburban", "Tahoe", "Silverado", "Chevelle"});
        models.put(makes[4], new String[] {"Viper", "RAM", "Neon", "Charger", "Durango"});
        HashMap<String, String> vinPrefix = new HashMap<>();
        vinPrefix.put(makes[0], "HOND-01");
        vinPrefix.put(makes[1], "TOYO-02");
        vinPrefix.put(makes[2], "FORD-03");
        vinPrefix.put(makes[3], "CHVY-04");
        vinPrefix.put(makes[4], "DOGE-05");

        for (int i = 0; i < 50; i++) {
            int nextYear = r.nextInt(40) + 1970;
            int nextMakeIndex = r.nextInt(makes.length);
            String nextMake = makes[nextMakeIndex];
            String[] nextModels = models.get(nextMake);
            String nextModel = nextModels[r.nextInt(nextModels.length)];
            String nextVin = String.format("%s%5s", vinPrefix.get(nextMake), i).replaceAll(" ", "0");
            Automobile auto = new Automobile(nextYear, nextMake, nextModel, nextVin);
            auto.setColor(colors[r.nextInt(colors.length)]);
            auto.setOwner(owners[i % owners.length]);
            this.testAutos.add(auto);
        }
        autosRepository.saveAll(this.testAutos);

        this.patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
    }

    @AfterEach
    void tearDown() {
        autosRepository.deleteAll();
    }

    @Test
    void doNothing() {
        for (Automobile auto : testAutos) {
            System.out.println(auto);
        }
        assertThat(true).isTrue();
    }

	@Test
	void contextLoads() {
	}

    @Test
    void getAutos_exists_returnsAutosList() {
        ResponseEntity<AutosList> response = restTemplate.getForEntity("/api/autos", AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
        for(Automobile auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void getAutos_search_returnsAutosList() {
        int seq = r.nextInt(50);
        String color = testAutos.get(seq).getColor();
        String make = testAutos.get(seq).getMake();
        ResponseEntity<AutosList> response = restTemplate.getForEntity(
                String.format("/api/autos?color=%s&make=%s", color, make), AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getAutomobiles().size()).isGreaterThanOrEqualTo(1);
        for(Object auto : response.getBody().getAutomobiles()){
            System.out.println(auto);
        }
    }

    @Test
    void getAutos_searchMake_returnsAutosList() {
        int seq = r.nextInt(50);
        String make = testAutos.get(seq).getMake();
        ResponseEntity<AutosList> response = restTemplate.getForEntity(
                String.format("/api/autos?make=%s", make), AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getAutomobiles().size()).isGreaterThanOrEqualTo(1);
        for(Object auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void getAutos_searchColor_returnsAutosList() {
        int seq = r.nextInt(50);
        String color = testAutos.get(seq).getColor();
        ResponseEntity<AutosList> response = restTemplate.getForEntity(
                String.format("/api/autos?color=%s", color), AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getAutomobiles().size()).isGreaterThanOrEqualTo(1);
        for(Object auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void addAuto_returnsNewAutoDetails() {
        Automobile automobile = new Automobile();
        automobile.setVin("ABC123XX");
        automobile.setYear(1992);
        automobile.setMake("Ferrari");
        automobile.setMake("F40");
        automobile.setColor("RED");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Automobile> request = new HttpEntity<>(automobile, headers);

        ResponseEntity<Automobile> response = restTemplate.postForEntity("/api/autos", request, Automobile.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    void getAutos_withVin_returnsAuto() {
        int seq = r.nextInt(50);
        String vin = testAutos.get(seq).getVin();
        ResponseEntity<Automobile> response = restTemplate.getForEntity(
                String.format("/api/autos/%s", vin), Automobile.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getVin()).isEqualTo(vin);
        System.out.println(response.getBody());
    }

    @Test
    void updateAuto_withObject_returnsAuto() throws JSONException {
        int seq = r.nextInt(50);
        String vin = testAutos.get(seq).getVin();
        String resourceUrl = "/api/autos/" + vin;

        JSONObject updateBody = new JSONObject();
        updateBody.put("color", "ORANGE");
        updateBody.put("owner", "Bob");

        ResponseEntity<Automobile> responseEntity = patchRestTemplate.exchange(resourceUrl, HttpMethod.PATCH, getPostRequestHeaders(updateBody.toString()), Automobile.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());

        Automobile updatedAuto = responseEntity.getBody();
        assertEquals("Bob", updatedAuto.getOwner());
        assertEquals("ORANGE", updatedAuto.getColor());

        System.out.println(responseEntity.getBody());
    }

    @Test
    void deleteAuto_withVin() {
        int seq = r.nextInt(50);
        String vin = testAutos.get(seq).getVin();
        ResponseEntity<Automobile> response = patchRestTemplate.exchange("/api/autos/" + vin, HttpMethod.DELETE, getPostRequestHeaders(""), Automobile.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    }

    public HttpEntity getPostRequestHeaders(String jsonPostBody) {
        List acceptTypes = new ArrayList();
        acceptTypes.add(MediaType.APPLICATION_JSON);

        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        reqHeaders.setAccept(acceptTypes);

        return new HttpEntity(jsonPostBody, reqHeaders);
    }

}
