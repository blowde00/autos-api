package com.statefarm.autos;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AutosController.class)
public class AutosControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AutosService autosService;

    ObjectMapper mapper = new ObjectMapper();

// GET: /api/autos
    // GET: /api/autos returns list of all autos in db
    @Test
    void getAutos_noParams_exists_returnsAutosList() throws Exception {
        // Arrange
        List<Automobile> automobiles = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            automobiles.add(new Automobile(1987 + i, "Ferrari", "F40", "AABB" + i));
        }
        when(autosService.getAutos()).thenReturn(new AutosList(automobiles));
        // Act
        mockMvc.perform(get("/api/autos"))
                .andDo(print())
                // Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
    }

    // GET: /api/autos no autos in db returns 204 no content
    @Test
    void getAutos_noParams_none_returnsNoContent() throws Exception {
        // Arrange
        when(autosService.getAutos()).thenReturn(new AutosList());
        // Act
        mockMvc.perform(get("/api/autos"))
                .andDo(print())
        // Assert
                .andExpect(status().isNoContent());
    }

    // GET: /api/autos?make=Ferrari&color=RED returns red Ferraris
    @Test
    void getAutos_searchParams_exists_returnsAutosList() throws Exception {
        // Arrange
        List<Automobile> automobiles = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            automobiles.add(new Automobile(1987 + i, "Ferrari", "F40", "AABB" + i));
        }
        when(autosService.getAutos(anyString(), anyString())).thenReturn(new AutosList(automobiles));
        // Act
        mockMvc.perform(get("/api/autos?color=YELLOW&make=Ferrari"))
        // Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
    }

    // GET: /api/autos?make=Ferrari returns Ferraris
    @Test
    void getAutos_searchParamsMake_exists_returnsAutosList() throws Exception {
        // Arrange
        List<Automobile> automobiles = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            automobiles.add(new Automobile(1987 + i, "Ferrari", "F40", "AABB" + i));
        }
        when(autosService.getAutos(eq(null), anyString())).thenReturn(new AutosList(automobiles));
        // Act
        mockMvc.perform(get("/api/autos?make=Ferrari"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
    }

    // GET: /api/autos?color=RED returns red cars
    @Test
    void getAutos_searchParamsColor_exists_returnsAutosList() throws Exception {
        // Arrange
        List<Automobile> automobiles = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            automobiles.add(new Automobile(1987 + i, "Ferrari", "F40", "AABB" + i));
        }
        when(autosService.getAutos(anyString(), eq(null))).thenReturn(new AutosList(automobiles));
        // Act
        mockMvc.perform(get("/api/autos?color=RED"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
    }

// POST: /api/autos
    // POST: /api/autos returns created automobile
    @Test
    void addAuto_valid_returnsAuto() throws Exception {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        when(autosService.addAuto(any(Automobile.class))).thenReturn(automobile);
        mockMvc.perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(automobile)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("make").value("Ferrari"));
    }

    // POST: /api/autos returns error message due to bad request (400)
    @Test
    void addAuto_badRequest_returns400() throws Exception {
        when(autosService.addAuto(any(Automobile.class))).thenThrow(InvalidAutoException.class);
        String json = "{\"year\":1992,\"make\":\"Ferrari\",\"model\":\"F40\",\"color\":null,\"owner\":null,\"vin\":\"AABBCC\"}";
        mockMvc.perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

// GET: /ai/autos/{vin}
    // GET: /api/autos/{vin} returns the requested automobile
    @Test
    void getAuto_withVin_ReturnsAuto() throws Exception {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCCDD");
        when(autosService.getAuto(anyString())).thenReturn(automobile);
        mockMvc.perform(get("/api/autos/" + automobile.getVin()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(automobile.getVin()));
    }

    // GET: /api/autos/{vin} returns 204 no content (vehicle not found)
    @Test
    void getAuto_withVin_none_returnsNoContent() throws Exception {
        doThrow(new AutoNotFoundException()).when(autosService).getAuto(anyString());
        mockMvc.perform(get("/api/autos/AABBCCDD"))
                .andExpect(status().isNoContent());
    }

// PATCH: /api/autos/{vin}
    // PATCH: /api/autos/{vin} returns patched automobile
    @Test
    void updateAuto_withObject_returnsAuto() throws Exception {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCCDD");
        when(autosService.updateAuto(anyString(), anyString(), anyString())).thenReturn(automobile);
        mockMvc.perform(patch("/api/autos/" + automobile.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"color\":\"RED\",\"owner\":\"Brandon\"}"))
        .andExpect(status().isOk());
        verify(autosService).updateAuto(anyString(), anyString(), anyString());
    }

    // PATCH: /api/autos/{vin} returns 204 no content (vehicle not found)
    @Test
    void patchAuto_withVin_notExists_returnsNoContent() throws Exception {
        doThrow(new AutoNotFoundException()).when(autosService).updateAuto(anyString(), anyString(), anyString());
        mockMvc.perform(patch("/api/autos/AABBCC")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"color\":\"RED\",\"owner\":\"Brandon\"}"))
                .andExpect(status().isNoContent());
    }

    // PATCH: /api/autos/{vin} returns 400 bad request (no payload, no changes, or already done?)
    @Test
    void patchAuto_badRequest_returns400() throws Exception {
        when(autosService.updateAuto(anyString(), anyString(), anyString())).thenThrow(InvalidAutoException.class);
        mockMvc.perform(patch("/api/autos/AABBCC")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"color\":\"RED\",\"owner\":\"Brandon\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

// DELETE: /api/autos/{vin}
    // DELETE: /api/autos/{vin} returns 202 accepted (delete request accepted)
    @Test
    void deleteAuto_withVin_exists_returns202() throws Exception {
        mockMvc.perform(delete("/api/autos/AABBCC"))
                .andExpect(status().isAccepted());
        verify(autosService).deleteAuto(anyString());
    }

    // DELETE: /api/autos/{vin} returns 204 no content (vehicle not found)
    @Test
    void deleteAuto_withVin_notExists_returnsNoContent() throws Exception {
        doThrow(new AutoNotFoundException()).when(autosService).deleteAuto(anyString());
        mockMvc.perform(delete("/api/autos/AABBCC"))
                .andExpect(status().isNoContent());
    }
}