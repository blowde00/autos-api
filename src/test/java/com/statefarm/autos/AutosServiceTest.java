package com.statefarm.autos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

class AutosServiceTest {
    private AutosService autosService;

    @Mock
    AutosRepository autosRepository;

    @BeforeEach
    void setUp() {
        autosService = new AutosService(autosRepository);
    }

    @Test
    void getAutos_noArgs_returnsList() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        when(autosRepository.findAll()).thenReturn(Arrays.asList(automobile));
        AutosList autosList = autosService.getAutos();
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_search_returnsList() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByColorContainsAndMakeContainsIgnoreCase(anyString(), anyString()))
                .thenReturn(Arrays.asList(automobile));
        AutosList autosList = autosService.getAutos("RED", "Ferrarri");
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_searchColorAndMake_noResults() {
        when(autosRepository.findByColorContainsAndMakeContainsIgnoreCase(anyString(), anyString())).thenReturn(List.of());
        assertThatExceptionOfType(AutoNotFoundException.class)
                .isThrownBy(() -> {
                    autosService.getAutos("PURPLE", "Brandon");
                });
    }

    @Test
    void getAutos_searchByColor_returnsList() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByColorContainsIgnoreCase(anyString()))
                .thenReturn(Arrays.asList(automobile));
        AutosList autosList = autosService.getAutos("RED", null);
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_searchByMake_returnsList() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByMakeContainsIgnoreCase(anyString()))
                .thenReturn(Arrays.asList(automobile));
        AutosList autosList = autosService.getAutos(null, "Ferrari");
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void addAuto_valid_returnsAuto() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.save(any(Automobile.class)))
                .thenReturn(automobile);
        Automobile auto = autosService.addAuto(automobile);
        assertThat(auto).isNotNull();
        assertThat(auto.getMake()).isEqualTo("Ferrari");
    }

    @Test
    void getAuto_withVin_returnsAuto() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));
        Automobile auto = autosService.getAuto(automobile.getVin());
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    void updateAutoColorAndOwner_withVin_returnsAuto() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));
        when(autosRepository.save(any(Automobile.class))).thenReturn(automobile);
        Automobile auto = autosService.updateAuto(automobile.getVin(), "YELLOW", "Brandon");
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    void deleteAuto_byVin() {
        Automobile automobile = new Automobile(1992, "Ferrari", "F40", "AABBCC");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));

        autosService.deleteAuto(automobile.getVin());

        verify(autosRepository).delete(any(Automobile.class));
    }

    @Test
    void deleteAuto_byVin_notExists() {
        when(autosRepository.findByVin(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(AutoNotFoundException.class)
                .isThrownBy(() -> {
                    autosService.deleteAuto("NOT-EXISTS-VIN");
                });
    }
}