# bl-simple-autos

## Project Details

### Build

```
./gradlew build
```

## Docker

*Note: Login to docker to push to hub.docker.com with:* `docker login`

### Build

```
docker build -t blowde00/bl-simple-autos:<version> .
```

### Push

```
docker push blowde00/bl-simple-autos:<version>
```

## Kubernetes Deployment

All YAML files are in the `k8s` directory.

### Namespace Reader

*Note: this is needed so our app can read the configmap in the cluster*

```
kubectl apply -f k8s/namespace-reader.yml
```

### Setup ConfigMap First

*Note: This is so we don't have to restart our deployment*

```
kubectl apply -f k8s/autos-configmap.yml
```

### Setup Service

```
kubectl apply -f k8s/autos-service.yml
```

### Deploy our Image!

```
kubectl apply -f k8s/autos-deployment.yml
```

### Interact with API

*Note: Should use a new tab for this as it is blocking.*

```
kubectl port-forward service/bl-simple-autos-service 9090:8080
```

Open your Browser to API Autos - http://localhost:9090/api/autos

Can access http://localhost:9090/h2-console with credentials in configmap.yml to look at data.

### Extras

View our `minikube` Cluster using the Dashboard.

*Note: Open in separate terminal as it is blocking.*

```
minikube dashboard
```

### Docker Registry Setup
Enter GitLab Deploy info into a k8s secret.

```
kubectl create secret docker-registry bl-simple-autos-gitlab-token \
    --docker-username="[username]" \
    --docker-password="[token]" \
    --docker-email="brandon.lowdermilk.wyp1@statefarm.com" \
    --docker-server="registry.gitlab.com"
```